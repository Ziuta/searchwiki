require('jquery-ui/themes/base/core.css');
require('jquery-ui/themes/base/menu.css');
require('jquery-ui/themes/base/autocomplete.css');
require('jquery-ui/themes/base/theme.css');

var $ = require('jquery');
var autocomplete = require('jquery-ui/ui/widgets/autocomplete');


$.renderItems = function(items, searchterm){
  var result = '';

  $(".suggestions-results").empty();
  $(".suggestions").hide();
  $("#results").empty();

  if (items.length < 1){
    result += "<div id='nothing'> No results found </div>"
  } else {
    result += "<ul id='collection'>";
    items.forEach(function(item, index){
      result += '<a href="'+ item.link+'" target="_blank"><div class="articles"><li><span class="titles">' + item.title + '</span>' + '<br>' + '<span class="intros">' + item.intro + '...</span></li></div></a>';
    });
    result += "</ul>";
  }

  $("#results").append(result);
  $(".suggestions").show();
}


$("#searchterm").autocomplete(
{
  minLength: 1,
  search: function () {console.log('searching'); },
  source: function (request, response)
  {
    $.ajax(
    {
      url: "https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=" + request.term + "&format=json&callback=?",
      dataType: "json",
      type: "Get",
      success: function (data)
      {
        response(data.query.search.slice(0, 5));
      },
      error: function(data) {
        console.log("Error %o", data);
      }
    });
  },
  select: function (event, ui)
  {
    var items = new Array();

    items.push({
      title: ui.item.title, 
      intro: ui.item.snippet, 
      link: "https://www.wikipedia.org/wiki/"+encodeURIComponent(ui.item.title),
    });

    $.renderItems(items, event.target.value); 
    $(".suggestions").hide();
    return false;
  }
}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .addClass("suggestion")
        .append( "<span class=\"suggestion-title\">" + item.title + "</span>" )
        .appendTo( ul );
}

$(document).ready(function(){
  
  function newSearch(){
    var query = $("#searchterm").val();
    
    $.ajax({
      url: "https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=" + query + "&format=json&callback=?",
      dataType: "json",
      type: "Get",
      success: function (data)
      {
        var items = new Array();

        if (data.query.length != 0) { 
          data.query.search.forEach(function(item, index){
              items.push({
                title: item.title, 
                intro: item.snippet, 
                link: "https://www.wikipedia.org/wiki/"+encodeURIComponent(item.title),
              });
          });
        }

        $.renderItems(items,query);
      },
      error: function(data) {
        console.log("Error %o", data);
      }
    });
  }

   
   document.getElementById("submit-button").addEventListener("click", newSearch);
   document.getElementById("searchterm").addEventListener("keypress", function(e){
    if(e.which == 13 || e.keyCode == 13){
      e.preventDefault();
      newSearch();
    }
  });
});
