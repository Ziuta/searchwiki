const path = require('path');
const webpack = require('webpack');
const Clean = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
// const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: [
  './js/scripts.js',
  './less/styles.less',
  './images/bg.jpg',
  ],
  output: {
    publicPath: __dirname + "/dist",
    // path: '/dist',
    path: __dirname + "/dist", //path.resolve(__dirname, "dist"), //__dirname + "/dist", //path.resolve(__dirname, "app/folder")
    filename: 'bundle.js'
  },
  module: {
  rules: [
    {
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      }
    },
    {
      test: /\.less$/,
      use: [{
        loader: "style-loader"
      }, {
        loader: "css-loader", options: {
          sourceMap: true
        }
      }, {
        loader: "less-loader", options: {
          sourceMap: true
        }
      }
      ]
    },{
        test: /\.css$/,
        use: [{
        loader: "style-loader"
      }, {
        loader: "css-loader", 
      }
      ]
      },
    { test: /\.(jpe?g|gif|png|svg|woff|ttf|wav|mp3)$/, loader: "file-loader" }
  ]
  },
  // plugins: [
  //   new Clean(
  //     ['dist'], 
  //     {root:__dirname}
  //   ),
  //   // new ExtractTextPlugin("app.[hash].css"),
  //   // new HtmlWebpackPlugin({
  //   //   title: 'jQuery UI Autocomplete demo, built with webpack'
  //   // })
  // ],
  devServer: {
    publicPath: __dirname + "/dist"
  }
};